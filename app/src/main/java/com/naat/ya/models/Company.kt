package com.naat.ya.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "company")
data class Company (
    @PrimaryKey(autoGenerate = true) var id: Long? = null,
    @SerializedName("empresa") @Expose var company: Int? = null,
    @SerializedName("usuario") @Expose var companyDescription: String? = null,
    @SerializedName("servicios") @Expose var refills: String? = null
)
