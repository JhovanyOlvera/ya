package com.naat.ya.models.db

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.naat.ya.models.Company
import com.naat.ya.models.db.persistence.CompanyDAO
import com.naat.ya.utils.yaAPP

@Database(
    entities = [Company::class],
    version = 1,
    exportSchema = false
)
abstract class YADataBase: RoomDatabase() {
    abstract fun companyDAO(): CompanyDAO

    companion object {
        @Volatile
        private var INSTANCE: YADataBase? = null

        fun getInstanceDB(): YADataBase {
            if (INSTANCE != null) {
                return INSTANCE as YADataBase
            }

            synchronized(this) {
                INSTANCE =
                    Room.databaseBuilder(yaAPP.ctx!!, YADataBase::class.java, "ya_db")
                        .fallbackToDestructiveMigration().build()
                return INSTANCE as YADataBase
            }

        }
    }
}