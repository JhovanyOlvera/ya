package com.naat.ya.models.db.persistence

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.naat.ya.models.Company

@Dao
interface CompanyDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCompanies(companies: List<Company>)

    @Query("DELETE FROM company")
    fun dropCompanies()

    @Query("SELECT * FROM company")
    fun getAllCompanies(): List<Company>

    @Query("SELECT * FROM company WHERE company = :company")
    fun getCompany(company : Int): List<Company>
}