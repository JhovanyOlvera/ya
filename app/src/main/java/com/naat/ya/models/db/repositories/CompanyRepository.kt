package com.naat.ya.models.db.repositories

import com.naat.ya.models.Company
import com.naat.ya.models.db.YADataBase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking

class CompanyRepository {

    companion object {
        private var instance = CompanyRepository()
        fun getInstance(): CompanyRepository {
            when (instance) {
                null -> instance = CompanyRepository()
            }
            return instance
        }

        private val userDAO by lazy {
            YADataBase.getInstanceDB().companyDAO()
        }
    }

    fun insertCompanies(companies: List<Company>) {
        return runBlocking(Dispatchers.IO) { userDAO.insertCompanies(companies) }
    }

    fun getAllCompanies():List<Company> {
        return runBlocking(Dispatchers.IO) { userDAO.getAllCompanies() }
    }
    fun getCompany(id : Int):List<Company> {
        return runBlocking(Dispatchers.IO) { userDAO.getCompany(id) }
    }
}