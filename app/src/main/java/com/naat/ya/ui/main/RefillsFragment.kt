package com.naat.ya.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.databinding.Observable
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.naat.ya.R
import com.naat.ya.adapters.CompanyAdapter
import com.naat.ya.interfaces.IRefillsView
import com.naat.ya.models.Company
import com.naat.ya.utils.Utils
import kotlinx.android.synthetic.main.fragment_main.*

class RefillsFragment : Fragment(), IRefillsView {

    private lateinit var vm: RefillsViewModel
    private lateinit var adapterCompany: CompanyAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm = ViewModelProvider.NewInstanceFactory().create(RefillsViewModel::class.java)
        vm.iRefillsView = this
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupRecyclerView()

        arguments?.let {
            when(it.get(ARG_SECTION_NUMBER)){
                0 -> {
                    vm.searchCompanies()
                    Utils().genericTextChangeListener(acactv_company, vm.ofCompany)
                    lister()
                }
                else -> {}
            }
        }


    }

    private fun lister() {
        vm.ofCompany.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (vm.hmCompany[vm.ofCompany.get()] != null) {
                    val item = vm.hmCompany[vm.ofCompany.get()]
                    vm.searchCompany(item!!)
                }
            }
        })
    }

    private fun setupRecyclerView() {
        adapterCompany= CompanyAdapter()
        rv_refills.layoutManager = LinearLayoutManager(context)
        rv_refills.setHasFixedSize(true)
        rv_refills.adapter = adapterCompany
    }

    companion object {
        private const val ARG_SECTION_NUMBER = "section_number"

        @JvmStatic
        fun newInstance(sectionNumber: Int): RefillsFragment {
            return RefillsFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }

    override fun setDataCompany(listCompanies: List<Company>) {
        adapterCompany.setData(listCompanies)
    }

    override fun setSpin(mlCompany: MutableList<String>) {
        acactv_company.setAdapter(ArrayAdapter<String>(context!!.applicationContext, R.layout.dropdown_item_line, mlCompany))

    }
}