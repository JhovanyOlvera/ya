package com.naat.ya.ui.login

import androidx.lifecycle.ViewModel
import com.naat.ya.api.APIClient
import com.naat.ya.api.rest.RestHttp
import com.naat.ya.interfaces.ILoginView
import com.naat.ya.utils.Utils

class LoginVM : ViewModel() {

    lateinit var iLoginView: ILoginView

    fun auth(user: String, pass: String) {
        val grant_type = "password"
        val client = APIClient.getAuthResource()
        RestHttp().auth(client, grant_type, user, pass.sha256(), iLoginView)
    }

}

private fun String.sha256(): String {
    return Utils().hashString(this, "SHA-256")
}


