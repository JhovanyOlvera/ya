package com.naat.ya.ui.main

import androidx.lifecycle.ViewModel
import com.naat.ya.models.Company
import com.naat.ya.models.db.repositories.CompanyRepository

class SplashViewModel : ViewModel() {

    fun fillDB() {
        if (CompanyRepository.getInstance().getAllCompanies().isEmpty()) {

            val listCompany : MutableList<Company> = mutableListOf()

            val companyClaro = Company()
            companyClaro.company = 1
            companyClaro.companyDescription = "CLARO"
            companyClaro.refills = "Tiempo aire|Megas|Megas"
            listCompany.add(companyClaro)

            val companyTuenti = Company()
            companyTuenti.company = 2
            companyTuenti.companyDescription = "TUENTI"
            companyTuenti.refills = "Tiempo aire|Tiempo aire|Tiempo aire"
            listCompany.add(companyTuenti)

            val companyEntel = Company()
            companyEntel.company = 3
            companyEntel.companyDescription = "ENTEL"
            companyEntel.refills = "Tiempo aire|Tiempo aire|Tiempo aire"
            listCompany.add(companyEntel)

            CompanyRepository.getInstance().insertCompanies(listCompany)
        }
    }


}


