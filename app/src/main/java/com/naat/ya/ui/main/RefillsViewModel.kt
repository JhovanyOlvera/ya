package com.naat.ya.ui.main

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.naat.ya.interfaces.IRefillsView
import com.naat.ya.models.Company
import com.naat.ya.models.db.repositories.CompanyRepository

class RefillsViewModel: ViewModel() {

    lateinit var iRefillsView : IRefillsView


    var hmCompany: HashMap<String, Company> = HashMap()
    var ofCompany: ObservableField<String?> = ObservableField()
    val mlCompany = mutableListOf<String>()

    fun searchCompanies() {
        iRefillsView.setDataCompany(CompanyRepository.getInstance().getAllCompanies())

        for(item in CompanyRepository.getInstance().getAllCompanies()){
            mlCompany.add(item.companyDescription!!)
            hmCompany[item.companyDescription!!] = item
        }
        iRefillsView.setSpin(mlCompany)
    }

    fun searchCompany(item: Company) {
        iRefillsView.setDataCompany(CompanyRepository.getInstance().getCompany(item.company!!))
    }

}