package com.naat.ya.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.naat.ya.R
import kotlinx.android.synthetic.main.activity_dialog_modal.*

class DialogModalActivity : AppCompatActivity() {

     private var  number = ""
     private var  amount = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dialog_modal)

        intent?.let{
           number = it.getStringExtra(KEY_NUMBER)!!
           amount = it.getStringExtra(KEY_AMOUNT)!!
        }

        tv_numberPhone.text = number
        tv_amount.text = " $${amount}.00"

        lister()
    }

    private fun lister() {
        btn_cancel.setOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }

        btn_accept.setOnClickListener{
            cv_preview.visibility = View.GONE
            cv_success.visibility = View.VISIBLE
        }

        btn_success_accept.setOnClickListener {
            setResult(Activity.RESULT_OK)
            finish()
        }

    }

    companion object {
        const val REQUEST_KEY = 0
        private const val KEY_NUMBER = "KEY_NUMBER"
        private const val KEY_AMOUNT = "KEY_AMOUNT"

        fun newIntent(ctx: Context, number : String, amount : String): Intent {
            return Intent(ctx, DialogModalActivity::class.java)
                .putExtra(KEY_NUMBER, number)
                .putExtra(KEY_AMOUNT, amount)

        }
    }
}
