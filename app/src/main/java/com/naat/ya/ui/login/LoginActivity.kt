package com.naat.ya.ui.login

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.naat.ya.MainActivity
import com.naat.ya.R
import com.naat.ya.interfaces.ILoginView
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), ILoginView {

    private lateinit var vm : LoginVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        vm = ViewModelProvider.NewInstanceFactory().create(LoginVM::class.java)
        vm.iLoginView = this@LoginActivity
        listener()
    }

    private fun listener() {
        btn_login.setOnClickListener {
            if (et_user.text!!.isNotEmpty() && et_password.text!!.isNotEmpty()) {
                pb_loader.visibility = View.VISIBLE
                vm.auth(et_user.text.toString(), et_password.text.toString())
            }
        }
    }

    companion object {
        fun newIntent(ctx: Context): Intent {
            return Intent(ctx, LoginActivity::class.java)
        }
    }

    override fun successLogin() {
        pb_loader.visibility = View.VISIBLE
        startActivity(MainActivity.newIntent(applicationContext))
    }

    override fun failedLogin() {
        pb_loader.visibility = View.GONE
        Toast.makeText(this, "Usuario o contraseña es incorrecta", Toast.LENGTH_LONG)
            .show()
    }
}
