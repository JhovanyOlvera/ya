package com.naat.ya.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.naat.ya.MainActivity
import com.naat.ya.R
import kotlinx.coroutines.*
import java.util.concurrent.TimeUnit

class SplashActivity : AppCompatActivity() {

    private lateinit var vm : SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        vm = ViewModelProvider.NewInstanceFactory().create(SplashViewModel::class.java)
        vm.fillDB()
        scheduleSplashScreen()
    }

    private fun scheduleSplashScreen() {
        CoroutineScope(Dispatchers.IO).launch {
            delay(TimeUnit.SECONDS.toMillis(5))
            withContext(Dispatchers.Main) {
                startActivity(MainActivity.newIntent(applicationContext))
            }
        }
    }

}


