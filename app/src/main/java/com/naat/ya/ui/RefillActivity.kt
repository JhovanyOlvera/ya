package com.naat.ya.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.naat.ya.R
import kotlinx.android.synthetic.main.activity_login.btn_login
import kotlinx.android.synthetic.main.activity_refill.*

class RefillActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_refill)

        supportActionBar?.let{
            it.setDisplayHomeAsUpEnabled(true)
            it.title = "RECARGAS"
        }
        listener()
    }

    private fun listener() {
        btn_login.setOnClickListener {
            if (et_numberPhone.text!!.isNotEmpty() && et_numberPhone.text!!.length == 10 && et_amount.text!!.isNotEmpty()) {
                startActivityForResult(DialogModalActivity.newIntent(applicationContext,et_numberPhone.text.toString(), et_amount.text.toString()), DialogModalActivity.REQUEST_KEY)
            }
        }
    }

    companion object {
        fun newIntent(ctx: Context): Intent {
            return Intent(ctx, RefillActivity::class.java)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == DialogModalActivity.REQUEST_KEY && resultCode == Activity.RESULT_OK) {
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> finish()
        }
        return true
    }

}
