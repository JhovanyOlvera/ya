package com.naat.ya.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.naat.ya.R
import com.naat.ya.models.Company
import com.naat.ya.ui.RefillActivity
import kotlinx.android.synthetic.main.item_company.view.*

class CompanyAdapter : RecyclerView.Adapter<CompanyAdapter.CompanyViewHolder>() {

    private val listCompany: MutableList<Company> = mutableListOf()

    fun setData(data: List<Company>) {
        this.listCompany.clear()
        this.listCompany.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompanyViewHolder {
        return CompanyViewHolder( LayoutInflater.from(parent.context).inflate( R.layout.item_company, parent, false))
    }

    override fun getItemCount(): Int {
        return this.listCompany.size
    }

    override fun onBindViewHolder(holder: CompanyViewHolder, position: Int) {
        holder.bindHistoricalAdvance(listCompany[position])
    }

    class CompanyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        fun bindHistoricalAdvance(company: Company) {

            when(company.company){
                1 -> { setImage(R.drawable.ic_claro) }
                2 -> { setImage(R.drawable.ic_tuenti) }
                3 -> { setImage(R.drawable.ic_entel) }
            }

            itemView.tv_company.text = company.companyDescription!!

            val itemRefill = company.refills!!.split("|")
            itemView.tv_title_1.text = itemRefill[0]
            itemView.tv_title_2.text = itemRefill[1]
            itemView.tv_title_3.text =itemRefill[2]

        }

        private fun setImage(image: Int ) {
            itemView.iv_proccess_1.setImageResource(image)
            itemView.iv_proccess_1.setOnClickListener(this)
            itemView.iv_proccess_2.setImageResource(image)
            itemView.iv_proccess_2.setOnClickListener(this)
            itemView.iv_proccess_3.setImageResource(image)
            itemView.iv_proccess_3.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            itemView.context.startActivity(RefillActivity.newIntent(itemView.context))
        }

    }
}
