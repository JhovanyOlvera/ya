package com.naat.ya.interfaces

interface ILoginView {

    fun successLogin()

    fun failedLogin()
}