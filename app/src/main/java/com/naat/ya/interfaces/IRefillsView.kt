package com.naat.ya.interfaces

import com.naat.ya.models.Company

interface IRefillsView {

    fun setDataCompany(listCompanies : List<Company>)

    fun setSpin(mlEquipments: MutableList<String>)

}