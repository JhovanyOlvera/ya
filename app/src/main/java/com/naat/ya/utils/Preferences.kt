package com.naat.ya.utils

import android.content.Context
import android.content.SharedPreferences

class Preferences(ctx: Context) {

    private val PREFS_APP = "com.ya.preferences"
    private val SHARED_ACCESS_TOKEN = "shared_access_token"
    private val SHARED_TOKEN_TYPE = "shared_token_type"
    private val SHARED_REFRESH_TOKEN = "shared_refresh_token"
    private val SHARED_EXPIRES_IN = "expires_in"
    private val SHARED_SCOPE = "scope"
    private val SHARED_JTI = "jti"

    private val prefs: SharedPreferences = ctx.getSharedPreferences(PREFS_APP, 0)

    var access_token : String
        get() = prefs.getString(SHARED_ACCESS_TOKEN, "")!!
        set(value) = prefs.edit().putString(SHARED_ACCESS_TOKEN, value).apply()

    var token_type : String
        get() = prefs.getString(SHARED_TOKEN_TYPE, "")!!
        set(value) = prefs.edit().putString(SHARED_TOKEN_TYPE, value).apply()

    var refresh_token : String
        get() = prefs.getString(SHARED_REFRESH_TOKEN, "")!!
        set(value) = prefs.edit().putString(SHARED_REFRESH_TOKEN, value).apply()

    var expires_in : String
        get() = prefs.getString(SHARED_EXPIRES_IN, "")!!
        set(value) = prefs.edit().putString(SHARED_EXPIRES_IN, value).apply()

    var scope : String
        get() = prefs.getString(SHARED_SCOPE, "")!!
        set(value) = prefs.edit().putString(SHARED_SCOPE, value).apply()

    var jti : String
        get() = prefs.getString(SHARED_JTI, "")!!
        set(value) = prefs.edit().putString(SHARED_JTI, value).apply()

}