package com.naat.ya.utils

class Constants {

    object Uri {
        const val BASE_URL = "https://uat.firmaautografa.com/"
    }

    object END_POINT {
        const val PROYECT_PATH = "authorization-server/"

        const val AUTH = "oauth/token"
    }

    object SECURITY {
        const val AUTHORIZATION = "Authorization"
        const val AUTHORIZATION_TOKEN = "Basic Wm1Ga0xXTXlZeTF3YjNKMFlXdz06TWpoa04yUTNNbUppWVRWbVpHTTBObVl4Wmpka1lXSmpZbVEyTmpBMVpEVXpaVFZoT1dNMVpHVTROakF4TldVeE9EWmtaV0ZpTnpNd1lUUm1ZelV5WWc9PQ=="
    }

   companion object  {
        const val EMPTY = ""
    }

}