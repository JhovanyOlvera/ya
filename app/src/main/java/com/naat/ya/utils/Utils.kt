package com.naat.ya.utils

import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.widget.AppCompatAutoCompleteTextView
import androidx.databinding.ObservableField
import java.security.MessageDigest

class Utils {

     fun hashString(input: String, algorithm: String): String {
        return MessageDigest
            .getInstance(algorithm)
            .digest(input.toByteArray())
            .fold("", { str, it -> str + "%02x".format(it) })
    }

    /**
     * <p>textChangeListener generico para componenter AppCompatAutoCompleteTextView.</p>
     * @return void - textChangeLister.
     */
    fun genericTextChangeListener(elementGeneric: AppCompatAutoCompleteTextView, ofGeneric: ObservableField<String?>) {
        elementGeneric.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                when {
                    !s.isNullOrBlank() -> ofGeneric.set(elementGeneric.text?.toString())
                    else -> ofGeneric.set(Constants.EMPTY)
                }
            }
        })
    }
}