package com.naat.ya.utils

import android.app.Application
import android.content.Context

class yaAPP : Application() {

    override fun onCreate() {
        super.onCreate()
        prefs = Preferences(applicationContext)
        ctx = applicationContext
    }

    companion object {
        lateinit var prefs: Preferences
        var ctx: Context? = null
    }

    override fun onTerminate() {
        super.onTerminate()
        if (ctx != null) {
            ctx = null
        }
    }
}