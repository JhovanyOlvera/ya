package com.naat.ya.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class JWT(
    @SerializedName("access_token") @Expose var access_token : String? = null,
    @SerializedName("token_type") @Expose var token_type : String? = null,
    @SerializedName("refresh_token") @Expose var refresh_token : String? = null,
    @SerializedName("expires_in") @Expose var expires_in : String? = null,
    @SerializedName("scope") @Expose var scope : String? = null,
    @SerializedName("jti") @Expose var jti : String? = null
)