package com.naat.ya.api

import com.naat.ya.utils.Constants.END_POINT.PROYECT_PATH
import com.naat.ya.utils.Constants.SECURITY.AUTHORIZATION
import com.naat.ya.utils.Constants.SECURITY.AUTHORIZATION_TOKEN
import com.naat.ya.utils.Constants.Uri.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class APIClient {

    companion object {

        private var retrofit: Retrofit? = null

        private var authResource: IAuthResource? = null

        private fun setupRetrofit() {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY

            val clientBuilder = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor { chain ->
                    var request = chain.request()
                    val header = request.headers().newBuilder()
                        .add(AUTHORIZATION, AUTHORIZATION_TOKEN).build()
                    request = request.newBuilder().headers(header).build()
                    chain.proceed(request)
                }
                .build()

            retrofit = Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .client(clientBuilder).baseUrl(BASE_URL + PROYECT_PATH).build()
        }


        private fun setupResources() {
            authResource = retrofit!!.create(IAuthResource::class.java)
        }

        fun getAuthResource(): IAuthResource {
            setupRetrofit()
            setupResources()
            return authResource!!
        }
    }
}