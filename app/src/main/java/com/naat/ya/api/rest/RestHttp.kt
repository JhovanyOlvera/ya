package com.naat.ya.api.rest

import com.naat.ya.api.IAuthResource
import com.naat.ya.api.JWT
import com.naat.ya.interfaces.ILoginView
import com.naat.ya.utils.Constants.Companion.EMPTY
import com.naat.ya.utils.yaAPP
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RestHttp {

    fun auth(client: IAuthResource, grantType: String, username: String, password: String, iLoginView: ILoginView) {

        val call = client.auth(grantType, username, password)
        call.enqueue(object : Callback<JWT> {

            override fun onResponse(call: Call<JWT>, response: Response<JWT>) {
                response.body()?.let { body ->

                    body.access_token?.let {
                        yaAPP.prefs.access_token = it
                    }

                    body.expires_in?.let {
                        yaAPP.prefs.expires_in = it
                    }

                    body.jti?.let{
                        yaAPP.prefs.jti = it
                    }

                    body.refresh_token?.let{
                        yaAPP.prefs.refresh_token = it
                    }

                    body.scope?.let{
                        yaAPP.prefs.scope = it
                    }

                    body.token_type?.let {
                        yaAPP.prefs.token_type = it
                    }

                    if(yaAPP.prefs.access_token.isNotBlank()){
                        iLoginView.successLogin()
                    }else{
                        iLoginView.failedLogin()
                    }
                }
            }

            override fun onFailure(call: Call<JWT>, t: Throwable) {
                yaAPP.prefs.access_token = EMPTY
                yaAPP.prefs.expires_in = EMPTY
                yaAPP.prefs.jti = EMPTY
                yaAPP.prefs.refresh_token = EMPTY
                yaAPP.prefs.scope = EMPTY
                yaAPP.prefs.token_type = EMPTY
                iLoginView.failedLogin()
            }
        })
    }
}