package com.naat.ya.api

import com.naat.ya.utils.Constants.END_POINT.AUTH
import retrofit2.Call
import retrofit2.http.*

interface IAuthResource {

    @FormUrlEncoded
    //@Headers("Authorization:Basic Wm1Ga0xXTXlZeTF3YjNKMFlXdz06TWpoa04yUTNNbUppWVRWbVpHTTBObVl4Wmpka1lXSmpZbVEyTmpBMVpEVXpaVFZoT1dNMVpHVTROakF4TldVeE9EWmtaV0ZpTnpNd1lUUm1ZelV5WWc9PQ==")
    @POST(AUTH)
    fun auth(
        @Field("grant_type") grant_type: String?,
        @Field("username") username: String?,
        @Field("password") password: String?
    ): Call<JWT>

}