package com.naat.ya

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.naat.ya.ui.login.LoginActivity
import com.naat.ya.ui.main.SectionsPagerAdapter
import com.naat.ya.utils.yaAPP
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)

        listener()

    }

    private fun listener() {
        iv_exit.setOnClickListener {
            yaAPP.prefs.access_token = ""
            onResume()
        }
    }

    override fun onResume() {
        super.onResume()
        if (yaAPP.prefs.access_token.isEmpty()) {
            startActivity(LoginActivity.newIntent(applicationContext))
            finish()
        }
    }

    companion object {
        fun newIntent(ctx: Context): Intent {
            return Intent(ctx, MainActivity::class.java)
        }
    }

}